#ifndef PSO_H
#define PSO_H

#include <iostream>
#include <valarray>
#include <vector>
#include <cassert>
#include <random>
#include <time.h>


template <typename T>
std::ostream& operator<<(std::ostream& os, const std::valarray<T>& array)
{
    const size_t count = array.size();
    if(count>0U)
    {
        for(size_t i=0U; i<count; ++i)
        {
            os<<array[i];
            if(i<(count-1U))
                os<<" ";
        }
    }
    else
    {
        os<<"[]";
    }

    return os;
}



namespace pso
{


template <typename value_t>
class Particle
{

public:
    Particle()
    {
        static_assert(std::is_floating_point<value_t>::value, "A particle must have floating point position");
    }

    Particle(const size_t dimensions)
    {
        static_assert(std::is_floating_point<value_t>::value, "A particle must have floating point position");

        setDimension(dimensions);
    }

    void setDimension(const size_t dim)
    {
        mDim = dim;
        mPos.resize(mDim);
        mVelocity.resize(mDim);
        mBestPos.resize(mDim);
    }

    template<typename OptimizationFunction>
    void evaluate(OptimizationFunction fitnessfunction)
    {
        assert(mDim>0U);

        mFitness = fitnessfunction(mPos);
        if(mFitness<=mBestFitness)
        {
            mBestFitness = mFitness;
            mBestPos = mPos;
        }
    }

    size_t dimension() const { return mDim; }

    value_t fitness() const { return mFitness; }
    value_t bestFitness() const { return mBestFitness; }

    const std::valarray<value_t>& position() const { return mPos; }
    const std::valarray<value_t>& velocity() const { return mVelocity; }
    const std::valarray<value_t>& bestPosition() const { return mBestPos; }

public:
    size_t mDim = 0U;
    value_t mFitness = std::numeric_limits<value_t>::max();
    value_t mBestFitness = std::numeric_limits<value_t>::max();

    std::valarray<value_t> mPos;
    std::valarray<value_t> mVelocity;
    std::valarray<value_t> mBestPos;
};

template<typename value_t>
static bool operator<(const Particle<value_t>& p1, const Particle<value_t>& p2)
{
    return p1.fitness() < p2.fitness();
}

template<typename value_t>
static size_t circularTopology(const size_t currParticleId,
                               const std::vector<Particle<value_t>>& particles,
                               const size_t bestParticleId)
{
    const size_t count = particles.size();
    const size_t left = currParticleId==0U ? (count-1U) : (currParticleId-1U);
    const size_t right = currParticleId==(count-1U) ? 0U : (currParticleId+1U);
    return particles[left]<particles[right] ? left : right;
}



/*
template<typename value_t,
         typename FitnessFunction,
         typename LowerBoundFunction,
         typename UpperBoundFunction,
         typename StopFunction,
         typename TopologyFunction>*/

template<typename value_t>
class PSO
{
public:
    using FitnessFunction = value_t(*)(const std::valarray<value_t>&);
    using LowerBoundFunction = value_t(*)(const size_t);
    using UpperBoundFunction = value_t(*)(const size_t);
    using StopFunction = bool(*)(const value_t, const size_t);
    using TopologyFunction = size_t(*)(const size_t,const std::vector<Particle<value_t>>&,const size_t);

    PSO(const size_t numDimensions,
        const size_t numParticles,
        FitnessFunction fitfunction,
        LowerBoundFunction lowerfunction,
        UpperBoundFunction upperfunction,
        StopFunction stopfunction) :
        mNumDimensions(numDimensions),
        mNumParticles(numParticles),
        mFitnessFunction(fitfunction),
        mLowerBoundFunction(lowerfunction),
        mUpperBoundFunction(upperfunction),
        mStopFunction(stopfunction),
        mTopologyFunction(circularTopology)
    {
    }

    PSO(const size_t numDimensions,
        const size_t numParticles,
        FitnessFunction fitfunction,
        LowerBoundFunction lowerfunction,
        UpperBoundFunction upperfunction,
        StopFunction stopfunction,
        TopologyFunction topologyFunction) :
        PSO(numDimensions, numParticles, fitfunction, lowerfunction, upperfunction, stopfunction)
    {
        mTopologyFunction = topologyFunction;
    }

    void init()
    {
        // Set initial epoch
        epoch = 0U;

        // Seed random generator
        mRng.seed(time(nullptr));

        // Setup particles
        mParticles.clear();
        mParticles.reserve(mNumParticles);
        for(size_t i=0U; i<mNumParticles; ++i)
            mParticles.emplace_back(mNumDimensions);

        // setup upper and lower bounds for each dimension
        mLowerBounds.resize(mNumDimensions);
        mUpperBounds.resize(mNumDimensions);
        for(size_t i=0U; i<mNumDimensions; ++i)
        {
            mLowerBounds[i] = mLowerBoundFunction(i);
            mUpperBounds[i] = mUpperBoundFunction(i);
        }

        // Setup initial position
        for(size_t i=0U; i<mNumParticles; ++i)
        {
            std::valarray<value_t>& p = mParticles[i].mPos;
            for(size_t j=0U; j<mNumDimensions; ++j)
                p[j] = (mUpperBounds[j] - mLowerBounds[j]) * mUnifDistr(mRng);

            // First fitness evaluation
            mParticles[i].evaluate(mFitnessFunction);
        }

        // Find best particle
        auto bestFitIt = std::min_element(mParticles.begin(), mParticles.end());
        mBestFitness = (*bestFitIt).fitness();
        mBestParticleId = std::distance(mParticles.begin(), bestFitIt);

        // Setup initial velocity
        for(size_t i=0U; i<mNumParticles; ++i)
        {
            const size_t bestId = mTopologyFunction(i, mParticles, mBestParticleId);
            const Particle<value_t>& bestNeighbor = mParticles[bestId];

            const std::valarray<value_t>& np = bestNeighbor.position();
            std::valarray<value_t>& p = mParticles[i].mPos;
            for(size_t j=0U; j<mNumDimensions; ++j)
            {
                value_t diff = mUpperBounds[j] - mLowerBounds[j]; //np[j] - p[j];
                p[j] = 2.0*diff * (mUnifDistr(mRng)-1.0)*2.0;
            }
        }
    }

    void run()
    {
        while(mStopFunction(mBestFitness, epoch)==false)
        {
            step();
            //if(epoch%10U==0U)
            //    std::cout<<"Fitness: "<<mBestFitness<<" Pos: "<<mParticles[mBestParticleId].mPos<<std::endl;
        }
    }

    void step()
    {
        epoch += 1U;

        // Update velocity
        for(size_t i=0U; i<mNumParticles; ++i)
        {
            const size_t bestId = mTopologyFunction(i, mParticles, mBestParticleId);
            Particle<value_t>& bestNeighbor = mParticles[bestId];

            std::valarray<value_t>& np = bestNeighbor.mPos;
            std::valarray<value_t>& p = mParticles[i].mPos;
            std::valarray<value_t>& bp = mParticles[i].mBestPos;
            std::valarray<value_t>& v = mParticles[i].mVelocity;
            for(size_t j=0U; j<mNumDimensions; ++j)
            {
                v[j] = mInertia*v[j] + mUnifDistr(mRng) * mLocalFactor * (bp[j]-p[j]) +
                                     + mUnifDistr(mRng) * mNeighborFactor * (np[j]-p[j]) ;
            }
        }

        // Update position
        for(size_t i=0U; i<mNumParticles; ++i)
        {
            std::valarray<value_t>& p = mParticles[i].mPos;
            p += mParticles[i].mVelocity;

            for(size_t j=0U; j<mNumDimensions; ++j)
                p[j] = std::max(mLowerBounds[j], std::min(p[j], mUpperBounds[j]));
        }

        // Fitness evaluation
        for(size_t i=0U; i<mNumParticles; ++i)
        {
            // First fitness evaluation
            mParticles[i].evaluate(mFitnessFunction);
        }

        // Find best fitness
        auto bestFitIt = std::min_element(mParticles.begin(), mParticles.end());
        mBestFitness = (*bestFitIt).fitness();
        mBestParticleId = std::distance(mParticles.begin(), bestFitIt);
    }

    const Particle<value_t>& bestParticle() const { return mParticles[mBestParticleId]; }

    value_t bestFitness() const { return mBestFitness; }

private:

    const size_t mNumDimensions;
    const size_t mNumParticles;

    value_t mBestFitness = std::numeric_limits<value_t>::max();
    size_t epoch;

    FitnessFunction mFitnessFunction;
    LowerBoundFunction mLowerBoundFunction;
    UpperBoundFunction mUpperBoundFunction;
    StopFunction mStopFunction;
    TopologyFunction mTopologyFunction;

    std::vector<Particle<value_t>> mParticles;
    size_t mBestParticleId;

    std::mt19937 mRng;
    std::uniform_real_distribution<value_t> mUnifDistr = std::uniform_real_distribution<value_t>(0.0, 1.0);

    std::valarray<value_t> mLowerBounds;
    std::valarray<value_t> mUpperBounds;

    std::valarray<value_t> mRandArray;

    const value_t mInertia = 0.7;
    const value_t mLocalFactor = 1.1;
    const value_t mNeighborFactor = 1.4;
};

}

#endif // PSO_H
