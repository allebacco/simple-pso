
#include "src/PSO.h"



float lowerBound(const size_t index)
{
    return -10.0;
}

float upperBound(const size_t index)
{
    return 10.0;
}

float fitness(const std::valarray<float>& pos)
{
    return std::pow(pos, 2.0f).sum();
}

bool stop(const float fit, const size_t epoch)
{
    return epoch>1000;
}



int main()
{
    pso::PSO<float> pSwarm(8, 1000, fitness, lowerBound, upperBound, stop);

    pSwarm.init();
    pSwarm.run();

    std::cout<<"Fitness: "<<pSwarm.bestFitness()<<"\nPos: "<<pSwarm.bestParticle().position()<<std::endl;

    return 0;
}

